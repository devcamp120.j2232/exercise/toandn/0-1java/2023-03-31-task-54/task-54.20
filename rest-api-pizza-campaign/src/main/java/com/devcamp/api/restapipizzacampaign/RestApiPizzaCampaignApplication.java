package com.devcamp.api.restapipizzacampaign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestApiPizzaCampaignApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestApiPizzaCampaignApplication.class, args);
	}

}
